#!/usr/bin/python2

proxyPort=443 #The port for the HTTP server to run on

woupdFile = open("cache/1_1_4_woupd.xml", "r").read()
sInfoFile = open("cache/fake_getservinfo.xml", "r").read()
postReply = open("cache/fake_login", "r").read()
gameCrcCk = open("cache/1_1_4_wz.xml", "r").read()

#print( woupdFile )
#print( sInfoFile )
#print( postReply )

######### End global variables ###########

from SocketServer import ThreadingMixIn
from urlparse import *
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
import os, sys, time, threading, urllib2, ssl

class ReqHandle(BaseHTTPRequestHandler):
	#protocol_version = 'HTTP/1.1' #These varibles are built into the BaseHTTPRequestHandler class
	server_version = 'cloudflare-nginx' #They just choose the HTTP version to send back
	sys_version = '' #And what the Server: header should be (Server: server_version sys_version)
	def do_POST(self):
		parsedUrl = urlparse(self.path)
		clHead = self.headers
		postLen = clHead.get("content-length")
		print "\nPOST Info: "+clHead.get("host")+parsedUrl.path+"\tLen: "+postLen
		postData = self.rfile.read( int(postLen) )
		if parsedUrl.path == "/WarZ/api/api_Login.aspx":
			print "Login Data: "+postData
		#print "Data: "+postData
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		#self.send_header("Content-length", sys.getsizeof(postReply))
		self.end_headers()
		legitToken = urllib2.urlopen( "https://"+clHead.get("host")+parsedUrl.path+"?"+postData ).read()
		#print "Token: "+legitToken
		self.wfile.write( legitToken )
		#self.wfile.write( postReply )

	def do_GET(self):
		parsedUrl = urlparse(self.path)
		clHead = self.headers
		#for k in clHead:
		#	print "K:"+k+"\t\tV:"+clHead[k]

		if clHead.has_key("host"):
			print "\nHost: "+clHead.get("host")

		if parsedUrl.path == "/":
			self.send_response(200)
			self.send_header('Content-type','text/html')
			self.end_headers()
			#print "INDEX!?"
			self.wfile.write("Proxy Connection Success!")
		elif parsedUrl.path == "/favicon.ico":
			self.send_response(404)
### War Z Handles Below #############################################################
		elif parsedUrl.path == "/wz/updater/woupd.xml":
			self.send_response(200)
			self.send_header('Content-type','text/xml')
			self.end_headers()
			self.wfile.write(woupdFile)
		elif parsedUrl.path == "/wz/api_getserverinfo.xml":
			self.send_response(200)
			self.send_header('Content-type','text/xml')
			self.end_headers()
			self.wfile.write(sInfoFile)
		elif parsedUrl.path == "/wz/wz.xml":
			self.send_response(200)
			self.send_header('Content-type','text/xml')
			self.end_headers()
			self.wfile.write(gameCrcCk)

### War Z Handles Above #############################################################
		else:
			self.send_response(200)
                        self.send_header('Content-type','text/html')
                        self.end_headers()
			print "Unhandled GET Request. Doing Remote GET"
			out = urllib2.urlopen( "https://"+clHead.get("host")+parsedUrl.path ).read()
                        self.wfile.write(out)


class ThreadHTTPServer(ThreadingMixIn, HTTPServer):
	pass

def main():
	try:
		server = ThreadHTTPServer(("", proxyPort), ReqHandle)
		server.socket = ssl.wrap_socket(server.socket, certfile='cert/server.pem', server_side=True)
		print "\n\tServer Started!"
		print "  Press CTRL+C To Stop Server\n\n"
		server.serve_forever()
	except KeyboardInterrupt:
		print "\n\nInterrupt Recieved, Shutting Down..."
		server.shutdown
		server.socket.close()
main()
