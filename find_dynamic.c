#include <windows.h>
#include <stdio.h>
#include <Tlhelp32.h>

/* This code will search for a set of bytes and return the address.
   Useful for finding dynamic pointers or text to be changed ect.
   Originally I created and used this for my League of Legends zoom hack.
*/

struct patchInfo {
	char* name; 		//Name of Patch
	char* mask; 		//Search mask. x is static, ? is dynamic.
	BYTE* searchData; 	//Bytes that the mask's x have to match
	BYTE* patchData; 	//Bytes to be written over the last $patchSize bytes.
	int patchSize; 		//Number of bytes in patchData
};
//0x90 is NOP
struct patchInfo S_TEST = {"Testing", "x??x??x", (BYTE*)"\xFF\x8B\x89", (BYTE*)"\x90\x90\x90\x8B", 4};
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

//Gives this executable debug privileges.
void giveDebugPrivs(){
	HANDLE token;
	TOKEN_PRIVILEGES privs;
	if(OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &token)){
		privs.PrivilegeCount = 1;
		LookupPrivilegeValue(NULL, "SeDebugPrivilege", &privs.Privileges[0].Luid);
		privs.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		AdjustTokenPrivileges( token, FALSE, &privs, sizeof(privs), NULL, NULL);
		CloseHandle(token);
	}
}

//Takes the name of the exe file, returns pid.
DWORD findProcess(char* name){
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	
	if (Process32First(snapshot, &entry) == TRUE){
		while(Process32Next(snapshot, &entry) == TRUE){
			if ( stricmp(entry.szExeFile, name) == 0){
				CloseHandle(snapshot);
				return entry.th32ProcessID;
			}
		}
	}
}

//The moduleEntry32 struct is for the baseAddr and size.
MODULEENTRY32 getModule(int pid){
	MODULEENTRY32 module;
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,pid);
	module.dwSize = sizeof(MODULEENTRY32);//MSDN says we have to set this to sizeof(MODULEENTRY32)
	
	if( snapshot == INVALID_HANDLE_VALUE ){
		CloseHandle(snapshot);
		printf("CreateToolhelp32Snapshot failed!\n");
	}
	
	if( Module32First(snapshot,&module) == FALSE ){
		CloseHandle(snapshot);
		printf("Module32First failed!\n");
	}
	printf("Module exe is: %s\n", module.szModule);
	CloseHandle(snapshot);
	return module;	
}

//Arguments; patch's struct name, the process handle to search, and the process module.
//Returns the address $patchSize bytes before the last search mask. 
//printf("Initial match\tMask:%c Mem:0x%x Search:0x%x\n", patch.mask[0], procMemory[i], patch.searchData[0]);
DWORD findAddr(struct patchInfo patch, HANDLE proc, MODULEENTRY32 module){
	BYTE* procMemory = malloc(module.modBaseSize);
	DWORD bytesRead, maskLen, sOffset;
	int found = 0;
	
	maskLen = strlen(patch.mask);

	ReadProcessMemory(proc, (void*)module.modBaseAddr, (void*)procMemory, module.modBaseSize, &bytesRead);
	
	for (DWORD i=0; i<bytesRead; ++i){// For loop searching all process memory
		if (procMemory[i] == patch.searchData[0]){
			found = 1; sOffset = 1; // We have the initial match
			
			for (DWORD a=1; a<maskLen; ++a){// For loop searching after initial match
				if ( patch.mask[a] == 'x' && procMemory[i+a] != patch.searchData[sOffset++] ){
					found = 0; break;
				}
			}
			
			if (found){
				printf("Found it!\t0x%x\n",module.modBaseAddr+i);
				free(procMemory); return (DWORD)module.modBaseAddr + i;
			}
		}
	}
	printf("Failed to find address.\n");
	free(procMemory);
}


int main(){
	DWORD pid, addr;
	HANDLE proc = NULL;
	MODULEENTRY32 module;
	
	giveDebugPrivs();
	
	pid = findProcess("test.exe");
	proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	module = getModule(pid);
	printf("PID: %i\tMODBADDR: 0x%x\tMODBSIZE: %i\n", pid, module.modBaseAddr, module.modBaseSize);
	addr = findAddr(S_TEST, proc, module);
	WriteProcessMemory(proc, (void*)addr, S_TEST.patchData, S_TEST.patchSize, NULL);
	
	CloseHandle(proc);
	return 0;
}