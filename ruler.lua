local pointOne = nil
local pointTwo = nil

concommand.Add("fgt_ruler", function(ply, cmd, args)
	if ((not pointOne) and (not pointTwo)) then
		pointOne = LocalPlayer():GetEyeTrace().HitPos
	elseif pointOne and (not pointTwo) then
		local pTwoTrace = LocalPlayer():GetEyeTrace()
		pointTwo = pTwoTrace.HitPos
		if pointOne == pointTwo then	-- If called without moving the aimpos then do a trace off the face of the object.
			local tr = {}
			tr.start = pointTwo
			tr.endpos = pointTwo + (pTwoTrace.HitNormal * 32768)
			tr.filter = {LocalPlayer()}
			pointTwo = util.TraceLine( tr ).HitPos
		end
		
		local dist = pointOne:Distance( pointTwo )
		print( string.format("(%f, %f, %f) to (%f, %f, %f)", pointOne.x, pointOne.y, pointOne.z, pointTwo.x, pointTwo.y, pointTwo.z) )
		LocalPlayer():ChatPrint( string.format("Distance: %f", dist) )
		
		pointOne = nil
		pointTwo = nil
	else
		LocalPlayer():ChatPrint("wtf....")
	end
end )
