code_snippets
=============

Various snippets of code writen in different languages.

find_dynamic.c will search through a program's memory and find a matching pattern and replace it.

ruler.lua is a script for garrysmod that will get the distance between two points, or off the face of the object.

tradingcard_scraper.py is a script I made to find which steam games, that supported trading cards, had the least amount of cards in a set. This was to aid in finding the cheapest way to gain levels in steam.

warz_login_proxy.py is a proxy https server for WarZ that would allow you to login when the login servers where down. (Which happened alot)
