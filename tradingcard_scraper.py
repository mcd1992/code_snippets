#!/usr/bin/env python
from bs4 import BeautifulSoup
import urllib.request
import re

appIdList = []
baseReqUrl = "http://store.steampowered.com/search/?category1=998&category2=29"
cardReqUrl = "http://steamcommunity.com/id/mcd1992/gamecards/"
user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36"

def getStorePage(pageNum):
	requestUrl = baseReqUrl+"&page="+str(pageNum)
	requestObj = urllib.request.Request(requestUrl)
	requestObj.add_header("User-agent", user_agent)
	storeHtml = urllib.request.urlopen(requestObj).read()
	return BeautifulSoup(storeHtml)

def getGameCardPage(appId):
	requestUrl = cardReqUrl+str(appId)
	requestObj = urllib.request.Request(requestUrl)
	requestObj.add_header("User-agent", user_agent)
	cardHtml = urllib.request.urlopen(requestObj).read()
	return BeautifulSoup(cardHtml)

def populateAppList():
	curPage = 1
	storeSoup = getStorePage(curPage)
	while True:
		for para in storeSoup.find_all('p'):
			if re.search('No results were returned for that query.', para.text):
				return
		for link in storeSoup.find_all('a'):
			if re.search('app', link['href']):
				#print( re.split('/', link['href'])[4] )
				appIdList.append( re.split('/', link['href'])[4] )
		curPage += 1
		storeSoup = getStorePage(curPage)

populateAppList()

for id in appIdList:
	numCards = 0
	cardSoup = getGameCardPage(id)
	for img in cardSoup.find_all('img', class_='gamecard'):
		numCards+=1

	print("ID: "+str(id)+" has "+str(numCards)+" cards.")


	"""for a in cardSoup.find_all('a', class_="whiteLink"):
		if re.search('gamecards/'+str(id), a['href']):
			cleanName = re.sub('\s+', '', a.text)
			print( cleanName )"""
